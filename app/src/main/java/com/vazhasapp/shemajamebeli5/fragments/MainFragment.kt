package com.vazhasapp.shemajamebeli5.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.vazhasapp.shemajamebeli5.adapter.ChildRecyclerAdapter
import com.vazhasapp.shemajamebeli5.adapter.RecyclerAdapter
import com.vazhasapp.shemajamebeli5.databinding.FragmentMainBinding
import com.vazhasapp.shemajamebeli5.viewModel.FieldsViewModel

class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    private val viewModel: FieldsViewModel by viewModels()
    private val myAdapter = RecyclerAdapter()
    private val myChildAdapter = ChildRecyclerAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
    }

    private fun init() {
        binding.recyclerView.apply {
            adapter = myAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
        listener()
    }

    private fun listener() {
        viewModel.fieldsLiveData.observe(viewLifecycleOwner, {
            myAdapter.setData(it.toMutableList()))
        })
        viewModel.childRecyclerLiveData.observe(viewLifecycleOwner, {
            myChildAdapter.setData(it.toMutableList())
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}