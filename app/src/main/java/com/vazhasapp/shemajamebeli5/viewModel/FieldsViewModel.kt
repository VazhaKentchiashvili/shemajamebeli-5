package com.vazhasapp.shemajamebeli5.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vazhasapp.shemajamebeli5.api.FieldsApiService
import com.vazhasapp.shemajamebeli5.model.JsonModel

class FieldsViewModel : ViewModel() {

    private var _fieldsLiveData = MutableLiveData<List<JsonModel>>()
    val fieldsLiveData: LiveData<List<JsonModel>> = _fieldsLiveData

    private var _childRecyclerLiveData = MutableLiveData<List<List<JsonModel>>>()
    val childRecyclerLiveData: LiveData<List<List<JsonModel>>> = _childRecyclerLiveData

    private suspend fun getResponse() {
        val result = FieldsApiService.getAllFields().getFields()
    }

}