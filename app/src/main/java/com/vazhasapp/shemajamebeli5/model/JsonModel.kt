package com.vazhasapp.shemajamebeli5.model

import com.google.gson.annotations.SerializedName

data class JsonModel(
    @SerializedName("field_id")
    val fieldId: Int?,
    @SerializedName("field_type")
    val fieldType: String?,
    val hint: String?,
    val icon: String?,
    @SerializedName("is_active")
    val isActive: Boolean?,
    val keyboard: String?,
    val required: String?
)