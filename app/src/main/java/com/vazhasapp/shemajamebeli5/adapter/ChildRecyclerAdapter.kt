package com.vazhasapp.shemajamebeli5.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vazhasapp.shemajamebeli5.databinding.RecyclerFieldsCardViewBinding
import com.vazhasapp.shemajamebeli5.model.JsonModel

class ChildRecyclerAdapter : RecyclerView.Adapter<ChildRecyclerAdapter.ChildRecyclerViewHolder>() {

    private val childRecyclerList = mutableListOf<List<List<JsonModel>>>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChildRecyclerViewHolder =
        ChildRecyclerViewHolder(
            RecyclerFieldsCardViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ChildRecyclerViewHolder, position: Int) {

    }

    override fun getItemCount() = childRecyclerList.size

    inner class ChildRecyclerViewHolder(private val binding: RecyclerFieldsCardViewBinding) :
        RecyclerView.ViewHolder(binding.root)


    fun setData(childList: MutableList<List<List<JsonModel>>>) {
        this.childRecyclerList.addAll(childList)
    }
}