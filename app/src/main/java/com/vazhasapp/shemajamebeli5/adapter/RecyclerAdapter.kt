package com.vazhasapp.shemajamebeli5.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vazhasapp.shemajamebeli5.databinding.RecyclerCardViewBinding
import com.vazhasapp.shemajamebeli5.model.JsonModel

class RecyclerAdapter : RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder>() {

    private val fieldsRecyclerList = mutableListOf<List<JsonModel>>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder =
        RecyclerViewHolder(
            RecyclerCardViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {

    }

    override fun getItemCount() = fieldsRecyclerList.size

    override fun getItemViewType(position: Int): Int {
        return super.getItemViewType(position)
    }

    inner class RecyclerViewHolder(binding: RecyclerCardViewBinding) :
        RecyclerView.ViewHolder(binding.root)

    fun setData(fieldsList: MutableList<List<JsonModel>>) {
        this.fieldsRecyclerList.clear()
        this.fieldsRecyclerList.addAll(fieldsList)
        notifyDataSetChanged()
    }
}