package com.vazhasapp.shemajamebeli5.api

import com.vazhasapp.shemajamebeli5.api.FieldsApiService.API_ENDPOINT
import com.vazhasapp.shemajamebeli5.model.JsonModel
import retrofit2.Response
import retrofit2.http.GET

interface FieldsApi {

    @GET(API_ENDPOINT)
    suspend fun getFields() : Response<List<List<JsonModel>>>

}