package com.vazhasapp.shemajamebeli5.api

import com.google.gson.Gson
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object FieldsApiService {
    private const val BASE_URL = "https://run.mocky.io/"
    const val API_ENDPOINT = "v3/d531f5f5-180d-4364-bae7-791dae87f732"

    fun getAllFields() : FieldsApi {
        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(FieldsApi::class.java)
    }
}